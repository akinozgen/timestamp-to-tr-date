MySql timestamp to Turkish date
-------------------------------

> MySql timestamp to Turkish semantic date format

Usage: timeStamp(string_timestamp, show_hide_clock, date_splitter, time_splitter)
```javascript
var myTime = timeStamp('2017-04-22 12:00', false)
>>> "22 Nsn, Cts"

var myTime = timeStamp('2017-04-22 12:00', true)
>>> "22 Nsn, Cts 12"

var myTime = timeStamp('2017/04/22 12.00', false, '/', '.')
>>> "22 Nsn, Cts 12"
```