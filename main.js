/**
 * @param (string) timestamp
 * @param (bool) showTime [false],
 * @param (string) dateSplitter ['-']
 * @param (string) timeSplitter [':']
 * @return (string)
 */

module.exports = (timestamp, showTime = false, dateSplitter = '-', timeSplitter = ':') => {

	let months = ['Ock', 'Şbt', 'Mrt', 'Nsn', 'Mys', 'Hzr', 'Tmz', 'Ağu', 'Eyl', 'Ekm', 'Ksm', 'Ark']
	let days   = ['Pzt', 'Sal', 'Çrş', 'Prş', 'Cum', 'Cts', 'Pzr']

	let split = timestamp.split(' '),
		date = String(split[0]).replace(dateSplitter, '-').replace(dateSplitter, '-'),
		time = String(split[1]).replace(timeSplitter, ':'),
		dateSplit = date.split('-'),
		timeSplit = time.split(':'),
		dateObj = new Date(date + ' ' + time)

	let beautifiedDate = Number.parseInt(dateSplit[2]).toString() + ' ' +
						months[Number.parseInt(dateSplit[1]) - 1] + ', ' +
						days[dateObj.getDay() - 1]

	let beautifiedTime = dateObj.getMinutes() == 0 ? dateObj.getHours().toPrecision()
							:
						dateObj.getHours().toPrecision() + ':' + dateObj.getMinutes().toPrecision()

	if ( ! showTime )
		return beautifiedDate
	else
		return beautifiedDate + ' ' + beautifiedTime

}